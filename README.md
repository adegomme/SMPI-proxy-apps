# SMPI Integration Testing

Proxy apps are scale models of real, massive HPC applications: each of
them exhibits the same communication and computation patterns than the
massive application that it stands for. But they last only a few
thousands lines instead of some millions of lines. These proxy apps
are usually provided for educational purpose, and also to ensure that
the represented large HPC applications will correctly work with the
next generation of runtimes and hardware. See also [this
explanation](http://lightsighter.org/posts/miniappredicament.html) of
the proxy apps' purpose.

SMPI is an implementation of MPI executing the application on top of
the SimGrid simulator. This allows to study the application behavior
in many details that would be hard to observe if not in a simulator,
or on platforms that do not exist yet. The predictive power of SMPI
was shown to be very good, provided that you correctly model the
target platform
[[&#128462;](https://hal.inria.fr/hal-01415484/file/smpi_article.pdf),
[&#128462;](https://hal.inria.fr/hal-01523608/file/predicting-energy-consumption-of-mpi-applications-with-a-single-node.pdf)]
while very scalable [[&#128462;](https://hal.inria.fr/hal-01544827/document)].

This repository gathers several collections of proxy apps, and reports
their support by the SMPI implementation. This is tested nightly on a
[dedicated jenkins server](https://ci.inria.fr/simgrid/job/SMPI-proxy-apps/build_mode=SMPI,label=proxy-apps/).

# Full Scale Applications

In addition to the proxy apps testing presented here, some real-scale
HPC projects directly use SMPI as a basis for their regression and
integration tests:

| Application                                                                                                         | Build status                                                                                                                        |
|---------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| [BigDFT](http://bigdft.org) is a DFT massively parallel electronic structure code using a wavelet basis set. | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SimGrid-BigDFT)](https://ci.inria.fr/simgrid/job/SimGrid-BigDFT/) |
| [StarPU](http://starpu.gforge.inria.fr/) is an unified runtime system for heterogeneous multicore architectures.    | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SimGrid-StarPU)](https://ci.inria.fr/simgrid/job/SimGrid-StarPU/) |
|                                                                                                                     |                                                                                                                                     |


# [ECP Proxy Application](https://proxyapps.exascaleproject.org/app/)

| Benchmark                          |   Lines | Lang        | SMPI    | Patch                                                 |
|------------------------------------|--------:|-------------|---------|-------------------------------------------------------|
| [AMG](ECP.org#amg)                 |   4,658 | C           | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_AMG)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_AMG/) | [:ticket:](src/ECP/AMG/patch_AMG.diff)                |
| [CLAMR](ECP.org#clamr)             | 109,477 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_CLAMR)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_CLAMR/) | [:ticket:](src/ECP/CLAMR/patch_clamr)                 |
| [Chatterbug](ECP.org#chatterbug)   |   1,050 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_Chatterbug)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_Chatterbug/) | :black_small_square:                                  |
| [Comb](ECP.org#comb)               |   1,965 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_Comb)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_Comb/) | [:ticket:](src/ECP/CoMD/patch_comb.diff)              |
| [CoMD](ECP.org#comd)               |   4,658 | C           | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_CoMD)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_CoMD/) | [:ticket:](src/ECP/CoMD/patch_CoMD.diff)              |
| [CoSP2](ECP.org#cosp2)             |   2,199 | C           | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_CoSP2)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_CoSP2/) | [:ticket:](src/ECP/CoSP2/patch_CoSP2.diff)            |
| [CloverLeaf](ECP.org#cloverleaf)   |  37,477 | C, F90      | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Mantevo_CloverLeaf)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Mantevo_CloverLeaf/) | [:ticket:](src/ECP/CloverLeaf/patch_CloverLeaf.diff)  |
| [EBMS](ECP.org#ebms)               |     841 | C++, F90    | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_EBMS)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_EBMS/) | :black_small_square:                                  |
| [ExaMiniMD](ECP.org#examinimd)     |   6,184 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_ExaMiniMD)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_ExaMiniMD/) | only if sampling is needed :  [:ticket:](src/ECP/ExaMiniMD/kokkos_shared.patch) [:ticket:](src/ECP/ExaMiniMD/ExaMiniMD_shared.patch)                                |
| [HPCCG](ECP.org#hpccg)             |   1,548 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_HPGMG)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_HPGMG/) | [:ticket:](src/ECP/HPCCG/patch_HPCCG.diff)            |
| [HPGMG](ECP.org#hpgmg)             |  12,073 | C           | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Mantevo_HPCCG)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Mantevo_HPCCG/) | :black_small_square:                                  |
| [Kripke](ECP.org#kriple)           |   5,350 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_kripke)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_kripke/) | [:ticket:](src/ECP/kripke/patch_kripke.diff)          |
| [MINITRI](ECP.org#minitri)         |   1,534 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_linearAlgebra)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_linearAlgebra/)) | :black_small_square:                                  |
| [MiniAero](ECP.org#miniaero)       |   4,645 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_miniAero)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_miniAero/) | [:ticket:](src/ECP/miniAero/patch_makefile.diff)      |
| [MiniAMR](ECP.org#miniamr)         |   8,329 | C           | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Mantevo_MiniAMR)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Mantevo_MiniAMR/) | :black_small_square:                                  |
| [MiniFE](ECP.org#minife)           |  19,907 | C           | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Mantevo_MiniFE)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Mantevo_MiniFE/) | [:ticket:](src/ECP/MiniFE/patch_MiniFE.diff)          |
| [MiniGhost](ECP.org#minighost)     |   8,962 | F90         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_MiniGhost)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_MiniGhost/) | [:ticket:](src/ECP/MiniGhost/patch_miniGhost_mk.diff) |
| [MiniGMG](ECP.org#minigmg)         |   4,257 | C           | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_MiniGMG)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_MiniGMG/) | :black_small_square:                                  |
| [MiniMD](ECP.org#minimd)           |   9,344 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_MiniMD)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_MiniMD/) | [:ticket:](src/ECP/MiniMD/patch_miniMD_Makefile.diff) |
| [MiniSMAC2D](ECP.org#minismac2d)   |   8,329 | F90         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Mantevo_MiniSMAC2D)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Mantevo_MiniSMAC2D/) | [:ticket:](src/ECP/MiniSMAC2D)                        |
| [MiniVite](ECP.org#minivite)       |   2,375 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_MiniVite)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_MiniVite/) | [:ticket:](src/ECP/MiniVite/patch_miniVite_mk.diff)   |
| [Nekbone](ECP.org#nekbone)         |  38,674 | C, F77      | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_Nekbone)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_Nekbone/) | :black_small_square:                                  |
| [MiniXyce](ECP.org#minixyce)       |   2,020 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_MiniXyce)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_MiniXyce/) | [:ticket:](src/ECP/MiniXyce/patch_MiniXyce.diff)      |
| [PENNANT](ECP.org#pennant)         |   3,464 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_PENNANT)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_PENNANT/) | [:ticket:](src/ECP/PENNANT)                           |
| [PETSC](ECP.org#petsc)             |  750,000| C, C++, F90         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_PETSC)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_PETSC/) | :black_small_square:                                    |
| [Picsar](ECP.org#picsar)           |  57,800 | F90         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_Picsar)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_Picsar/) | [:ticket:](src/ECP/Picsar/patch_picsar.diff)          |
| [ProfugusMC](ECP.org#profugusmc)   | 107,400 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_ProfugusMC)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_ProfugusMC/) | :black_small_square:                                  |
| [Quicksilver](ECP.org#quicksilver) |   9,821 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_quicksilver)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_quicksilver/) | [:ticket:](src/ECP/Quicksilver)                       |
| [SimpleMOC](ECP.org#simplemoc)     |   2,864 | C           | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_SimpleMOC)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_SimpleMOC/) | [:ticket:](src/ECP/SimpleMOC/patch_SimpleMOC.diff)    |
| [SNAP](ECP.org#snap)               |   4,640 | F90         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_SNAP)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_SNAP/) | :black_small_square:                                  |
| [SNBONE](ECP.org#snbone)           |   5,897 | F90         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_SNbone)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_SNbone/) | [:ticket:](src/ECP/SNbone)                            |
| [SWFFT](ECP.org#swfft)             |   3,827 | C++, C, F90 | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_SWFFT)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_SWFFT/) | only if sampling is needed : [:ticket:](src/ECP/SWFFT/sampling.patch), only for shared allocs : [:ticket:](src/ECP/SWFFT/shared_alloc.patch)                                  |
| [Sw4lite](ECP.org#sw4lite)         |  48,436 | C           | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_sw4lite)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_sw4lite/) | only if sampling is needed : [:ticket:](src/ECP/sw4lite/sampling.patch), only for shared allocs : [:ticket:](src/ECP/sw4lite/shared_alloc.patch)                                  |
| [TeaLeaf](ECP.org#tealeaf)         |   5,729 | F90         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_TeaLeaf)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_TeaLeaf/) | [:ticket:](src/ECP/TeaLeaf/patch_tealeaf.diff)        |
| [Tycho2](ECP.org#tycho2)           |   6,343 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_Tycho2)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_Tycho2/) | [:ticket:](src/ECP/tycho2/patch_tycho2.diff)          |
| [VPFFT](ECP.org#vpfft)             |   3,384 | C++         | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_VPFFT)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_VPFFT/) | [:ticket:](src/ECP/VPFFT/patch_vpfft.diff)            |
| [XSBench](ECP.org#xsbench)         |   1,086 | C (OMP)     | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-ECP_XSBench)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-ECP_XSBench/) | :black_small_square:                           |

The following apps of this collection are not included:
  - Requiring OpenMP only : PathFinder, CoHMM, NuT, RSBench, CoGL
  - Laghos (depends on HYPRE, METIS, MFEM)
  - FleCSALE (depends on Exodus, FleCSI, ParMETIS)
  - PlasmApp (depends on Tirilinos)
  - RajaPerformanecesuite (requires MPI calls not supported by SMPI)
  - Clover3D (compiling issues)
  - LCALS, AMR_Exp_Parabolic (code source not available)
  - ASPA : (serial code)
# [CORAL benchmarks](https://asc.llnl.gov/CORAL-benchmarks/)

| Benchmark                      | Lines  | Lang           | SMPI    | Patch                                                                                 |
|--------------------------------|-------:|----------------|---------|---------------------------------------------------------------------------------------|
| [amg2013](Coral.org#amg2013)   | 75,000 | C              | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Coral_AMG2013)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Coral_AMG2013/) | [:ticket:](src/Coral/AMG2013/patch_AMG2013.diff)                                      |
| [HACCIO](Coral.org#hacc_io)    | 2,000  | C++            | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Coral_HACC_IO)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Coral_HACC_IO/) | [:ticket:](src/Coral/HACC_IO/patch_HACCIO.diff)                                       |
| [KMI_HASH](Coral.org#kmi_hash) | 4,239  | C              | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Coral_KMI_HASH)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Coral_KMI_HASH/) | [:ticket:](https://framagit.org/simgrid/SMPI-proxy-apps/tree/master/src/Coral/kmi_hash) |
| [LULESH](Coral.org#lulesh)     | 5,000  | C              | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Coral_Lulesh)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Coral_Lulesh/) | [:ticket:](src/Coral/Lulesh/patch_lulesh.diff)                                        |
| [IOR](Coral.org#IOR)           | 14,000  | C             | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Coral_IOR)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Coral_IOR/) | :black_small_square:                                                                  |
| [UMT2013](Coral.org#UMT2013)   | 34,000  | C, F90, C++   | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Coral_UMT2013)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Coral_UMT2013/) | [:ticket:](src/Coral/UMT2013/make.defs)                                                                  |

The following apps of this collection are not included:
- Requiring OpenMP: MCB
  ([:book:](https://asc.llnl.gov/CORAL-benchmarks/Summaries/MCB_Summary_v1.1.pdf)
  [:package:](https://asc.llnl.gov/CORAL-benchmarks/Throughput/mcb-20130723.tar.gz))
  QMCPACK
  ([:book:](https://asc.llnl.gov/CORAL-benchmarks/Summaries/QMCPACK_Summary_v1.2.pdf)
  [:package:](https://asc.llnl.gov/CORAL-benchmarks/Throughput/qmcpack-coral20131203.tar.gz)),
  integer_sort
  ([:book:](https://asc.llnl.gov/CORAL-benchmarks/Summaries/BigSort_Summary_v1.1.pdf)
  [:package:](https://asc.llnl.gov/CORAL-benchmarks/Datacentric/BigSort-20130808.tar.bz2))
  CLOMP
  ([:book:](https://asc.llnl.gov/CORAL-benchmarks/Summaries/CLOMP_Summary_v1.2.pdf)
  [:package:](https://asc.llnl.gov/CORAL-benchmarks/Skeleton/clomp_v1.2.tar.gz)),
  Graph500, SPECint2006"peak", Memory benchmarks,
  CLOMP, FTQ, XSBench, NEKbonemk, HACCmk, UMTmk, AMGmk, MILCmk,
  GFMCmk.
- Using Python: Pynmaic.
- LSMS ([:book:](https://asc.llnl.gov/CORAL-benchmarks/Summaries/LSMS_Summary_v1.1.pdf)[:package:](https://asc.llnl.gov/CORAL-benchmarks/Science/LSMS_3_rev237.tar.bz2))
  Depends on HDF5
- QBOX ([:book:](https://asc.llnl.gov/CORAL-benchmarks/Summaries/QBox_Summary_v1.2.pdf)[:package:](https://asc.llnl.gov/CORAL-benchmarks/Science/qball_r140b.tgz))
  Depends on OpenMP, Blas, LAPACK.
- CAM-SE ([:book:](https://asc.llnl.gov/CORAL-benchmarks/Summaries/CAMSE_Summary_v1.1.pdf)[:package:](https://asc.llnl.gov/CORAL-benchmarks/Throughput/homme1_3_6_mira_2.tgz))
  Depends on NETCDF, Lapack, cBlas.
- NAMD ([:book:](https://asc.llnl.gov/CORAL-benchmarks/Summaries/NAMD_Summary_v1.0.pdf)[:package:](https://asc.llnl.gov/CORAL-benchmarks/Throughput/namd-src.tar.gz))
  Requires mpixlc.


# [Trinity-Nersc benchmarks](http://www.nersc.gov/users/computational-systems/cori/nersc-8-procurement/trinity-nersc-8-rfp/nersc-8-trinity-benchmarks/) 

  
| Benchmark                                              |  Lines  | Lang     | SMPI    | Patch                                                                |
|--------------------------------------------------------|--------:|--------- |---------|----------------------------------------------------------------------|
| [SMB - mpiheader](Trinity-Nersc.org#smb_mpioverheader) |     418 | C        | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_SMB_mpiHeader)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_SMB_mpiHeader/) | [:ticket:](src/Trinity-Nersc/smb/mpi_overhead)                       |
| [SMB - msgrate](Trinity-Nersc.org#smb_msgrate)         |     362 | C        | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_SMB_msgrate)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_SMB_msgrate/) | [:ticket:](src/Trinity-Nersc/smb/msgrate/patch_MsgrateMakefile.diff) |
| [ziatest](Trinity-Nersc.org#ziatest)                   |     255 | C        | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_ziatest)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_ziatest/) | [:ticket:](src/Trinity-Nersc/ziatest)                                |
| [psnap](Trinity-Nersc.org#psnap)                       |     384 | C        | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_psnap)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_psnap/) | :black_small_square:                                                 |
| [mdtest](Trinity-Nersc.org#mdtest)                     |   2,187 | C        | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_mdtest)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_mdtest/) | [:ticket:](src/Trinity-Nersc/mdtest/patch_mdtest.diff)               |
| [mpimemu](Trinity-Nersc.org#mpimemu)                   |   4,980 | C        | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_mpimemu)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_mpimemu/) | :black_small_square:                                                 |
| [OMB_MPI (pt2pt)](Trinity-Nersc.org#pt2pt)             |   1,406 | C        | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_OMB_MPI_pt2pt)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_OMB_MPI_pt2pt/) | :black_small_square:                                                 |
| [OMB_MPI (one-sided)](Trinity-Nersc.org#one-sided)     |   1,679 | C        | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_OMB_MPI_one-sided)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_OMB_MPI_one-sided/) | :black_small_square:                                                 |
| [OMB_MPI (collective)](Trinity-Nersc.org#collective)   |   1,269 | C        | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_OMB_MPI_collective)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_OMB_MPI_collective/) | :black_small_square:                                                 |
| [MiniFE](Trinity-Nersc.org#minife)                     |   4,968 | C++      | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_MiniFE)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_MiniFE/) | [:ticket:](src/Trinity-Nersc/MiniFE/patch_miniFE.diff)               |
| [GTC](Trinity-Nersc.org#gtc)                           |   5,591 | F90      | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_GTC)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_GTC/) | [:ticket:](src/Trinity-Nersc/GTC/patch_gtc.diff)                     |
| [MILC](Trinity-Nersc.org#milc)                         |  82,645 | C        | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_MILC)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_MILC/) | [:ticket:](src/Trinity-Nersc/MILC/patch_MILC.diff)                   |
| [MiniDFT](Trinity-Nersc.org#minidft)                   |  30,874 | C, F90   | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_MiniDFT)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_MiniDFT/) | (with MKL)                                                           |
| [Stream](Trinity-Nersc.org#stream)                     |     747 | C, F77   | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-Trinity_stream)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-Trinity_stream/) | [:ticket:](src/Trinity-Nersc/MILC/patch_stream_mk.diff)                   |

The following apps of this collection are not included:
- Requiring OpenMP: psnap
  ([:book:](http://www.nersc.gov/users/computational-systems/cori/nersc-8-procurement/trinity-nersc-8-rfp/nersc-8-trinity-benchmarks/psnap/)[:package:](http://www.nersc.gov/assets/Trinity--NERSC-8-RFP/Benchmarks/June28/psnap-1.2June28.tar)),
- UPC-FC
  ([:book:](http://www.nersc.gov/users/computational-systems/cori/nersc-8-procurement/trinity-nersc-8-rfp/nersc-8-trinity-benchmarks/npb-upc-ft/)[:package:](http://www.nersc.gov/assets/Trinity--NERSC-8-RFP/Benchmarks/Jan9/UPC-FT.tar)):
  Depending on UPC
- Using Python: UMT.

# [CodeVault benchmarks](https://repository.prace-ri.eu/git/PRACE/CodeVault)
| Benchmark                                  |  Lines | Lang | SMPI    | Patch                                                         |
|--------------------------------------------|-------:|------|---------|---------------------------------------------------------------|
| [MCM](CodeVault.org#mcm)                   |    453 | C++  | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-CodeVault_MCM)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-CodeVault_MCM/) | [:ticket:](src/CodeVault/monte_carlo_methods)                 |
| [n-Body_methods](CodeVault.org#dyn-sparse) |  2,401 | C++  | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-CodeVault_DynSparse)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-CodeVault_DynSparse/) | [:ticket:](src/CodeVault/n-body_methods/patch_dynSparse.diff) |
| [basic MPI IO](CodeVault.org#basicMPIIO)   |    380 | C++  | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-CodeVault_parallelio)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-CodeVault_parallelio/) | [:ticket:](src/CodeVault/parallel_io/patch_basicMPIIO.diff) |
| [read2shmem](CodeVault.org#read2shmem)     |    202 | C++  | [![Build Status](https://ci.inria.fr/simgrid/buildStatus/icon?job=SMPI-proxy-app-CodeVault_read2shmem)](https://ci.inria.fr/simgrid/view/Proxy%20Apps/job/SMPI-proxy-app-CodeVault_read2shmem/) | [:ticket:](src/CodeVault/parallel_io/patch_read2shmem.diff) |

The following apps of this collection are not included:

- Requesting OpenMP: Dense_linear_algebra, N-body_methods (bhtree, hermite4, naive), Structured_grids.
- Using unsupported MPI primitives : Unstructed_grid (halo exchange)
- Sparse_linear_algebra (depends on PETSc)
- Spectral_methods (depends on OpenMP, CUDA, OpenCL, FFTW)
- Unstructured_grids ( depends on libmesh)

Issues : n-body_methods-bhtree_mpi (issue with Body.cpp)

# [MeteoFrance Proxy Applications](https://zenodo.org/record/1066934#.WyImghyxU5l)

| Benchmark                              | Lines | Lang | SMPI    | Patch                                                                                        |
|----------------------------------------|------:|------|---------|----------------------------------------------------------------------------------------------|
| [GCR](MeteoFrance.org#gcr)             |   504 | C++  | :sunny: | :black_small_square:                                                                         |
| [Halo](MeteoFrance.org#halo)           |  1910 | C++  | :sunny: | [:ticket:](https://framagit.org/simgrid/SMPI-proxy-apps/tree/master/src/MeteoFrance/halo)      |
| [Transpose](MeteoFrance.org#transpose) |  2554 | C++  | :sunny: | [:ticket:](https://framagit.org/simgrid/SMPI-proxy-apps/tree/master/src/MeteoFrance/transpose) |

# [HPL](http://www.netlib.org/benchmark/)

| Benchmark                     |  Lines | Lang | SMPI    | Patch                  |
|-------------------------------|--------|------|---------|------------------------|
| [Vanilla](HPL.org#vanilla)    | 15,807 | C    | :sunny: | :black_small_square:   |

# Legend
| Symbol         | Meaning                                  | Symbol               | Meaning              |
|----------------|------------------------------------------|----------------------|----------------------|
| :sunny:        | Execution successful                     | :ticket:             | Patch provided       |
| :x:            | Execution fails                          | :black_small_square: | No patch needed      |
| :partly_sunny: | Done successfully but with some warnings | :construction:       | SMPI missing feature |
| :beetle:       | Has an open issue                        |                      |                      |
| :book:         | Compilation instructions                 | :package:            | Source code          |
